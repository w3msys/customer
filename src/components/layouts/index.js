import header from './header.vue';
import sideBar from './side-bar.vue';
import master from './master.vue';
import headerHotel from './header-hotel.vue';
import headerNormalSearch from './header-normal-search.vue';
import search from './search.vue';
import headerAdvancedSearch from './header-advance-search.vue';

export const AppHeader = header;
export const AppSideBar = sideBar;
export const Master = master;
export const AppHeaderHotel = headerHotel;
export const AppHeaderNormalSearch = headerNormalSearch;
export const Search = search;
export const AppHeaderAdvancedSearch = headerAdvancedSearch;