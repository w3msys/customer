import welcome from './authentication/welcome.vue';
import notfound from './errors/not-found-404.vue';
import login from './authentication/login.vue';
import home from './authentication/home.vue';
import register from './authentication/register.vue';
import forgot_password from './authentication/forgot-password.vue';
import reset_password from './authentication/reset-password.vue';
import my_account from './account/my-account.vue';
import my_bookings from './bookings/my-bookings.vue';
import dashboard from './dashboard/dashboard.vue';
import hotelpage from './hotel/hotel.vue';

import normalSearch from'./search/normal-search.vue';
import advanceSearch from './search/advance-search.vue';

import bookItemHistory from './bookings/book-item-history.vue';
import bookItemNormal from './bookings/book-item-normal.vue';


export const Welcome = welcome;
export const NotFound = notfound;
export const Login = login;
export const Home = home;
export const Register = register;
export const ForgotPassword = forgot_password;
export const ResetPassword = reset_password;
export const MyAccount = my_account;
export const MyBookings = my_bookings;
export const Dashboard = dashboard;
export const BookItemNormal = bookItemNormal;
export const BookItemHistory = bookItemHistory;
export const HotelPage = hotelpage;
export const NormalSearch = normalSearch;
export const AdvanceSearch = advanceSearch;