export default {
    install (Vue, {name, storageEngine}) {

        Vue.user = {
            login (token) {
                if (!token) throw new Error('token is required');
                storageEngine.setItem(name, token);
                return true;
            },

            getToken () {
                let token = storageEngine.getItem(name);
                return token || null
            },

            isAuthenticated () {
                return !!this.getToken() || false 
            },

            logout () {
                storageEngine.removeItem(name);
                return true
            }
        }

        Vue.prototype.$user = Vue.user;
    }
}