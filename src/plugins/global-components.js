import {Logo, IconUser, IconPassword, IconEye, IconSearch, IconLocation, IconBook, IconHome, IconAccount, IconStar} from './../components/commons';
import {AppHeader, AppSideBar, AppHeaderHotel, AppHeaderNormalSearch, AppHeaderAdvancedSearch} from './../components/layouts';
import { BookItemNormal, BookItemHistory } from '../components/views';


const GlobalComponents = {
    install (Vue) {
      //register icons
      Vue.component('app-logo', Logo);
      Vue.component('app-icon-user', IconUser);
      Vue.component('app-icon-password', IconPassword);
      Vue.component('app-icon-search', IconSearch);
      Vue.component('app-icon-eye', IconEye);
      Vue.component('app-icon-location', IconLocation);
      Vue.component('app-icon-book', IconBook);
      Vue.component('app-icon-home', IconHome);
      Vue.component('app-icon-account', IconAccount);
      Vue.component('app-icon-star', IconStar);

      //register other components
      Vue.component('app-header', AppHeader);
      Vue.component('app-header-hotel', AppHeaderHotel);
      Vue.component('app-header-normal-search', AppHeaderNormalSearch);
      Vue.component('app-sidebar', AppSideBar);
      Vue.component('app-book-item-normal', BookItemNormal);
      Vue.component('app-book-item-history', BookItemHistory);
      Vue.component('app-header-advanced-search', AppHeaderAdvancedSearch);
    }
  }
  
  export default GlobalComponents