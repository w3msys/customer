import AuthPlugin from './auth-plugin';
import GlobalComponents from './global-components';
import VeeValidate from 'vee-validate';

export default {
  install (Vue) {
    Vue.use(AuthPlugin, {name: 'pelum-customer', storageEngine: sessionStorage});
    Vue.use(GlobalComponents);
    Vue.use(VeeValidate);
  }
}