import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes';


Vue.use(Router);

// NProgress.start()

const router = new Router({
  routes, mode: 'history'
})

router.afterEach((to, from) => {
  let title = document.getElementsByTagName('title')[0]
  title.innerText = 'Pelum | '
  title.innerText +=  to.meta.title ? to.meta.title : 'Not Found - 404'
})

export default router