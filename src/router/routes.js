import {Welcome, NotFound, Login, Register, ForgotPassword, ResetPassword, MyAccount, MyBookings, Dashboard, HotelPage, NormalSearch, AdvanceSearch} from './../components/views';
import {AppHeader, Master, Search} from './../components/layouts';


export default [
    {
      path: '/',
      name: 'welcome',
      component: Welcome,
      meta: {title: 'Welcome to Pelum'}
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {title: 'Login to Your Account'}
    },
    {
      name: 'register',
      path: '/register',
      component: Register,
      meta: {title: 'Create an account with us'}
    },
    {
      name: 'forgot_password',
      path: '/forgot-password',
      component: ForgotPassword,
      meta: {title: 'Forgot Password ? Dont worry, We got you covered'}
    },
    {
      name: 'reset_password',
      path: '/reset-password',
      component: ResetPassword,
      meta: {title: 'Reset your password'}
    },
    {
      name: 'user',
      path: '/user',
      component: Master,
      meta: {title: 'Welcome to your Dashboard'},
      children: [
        {
          name: 'my_account',
          path: 'account',
          component: MyAccount,
          meta: {title: 'View Your account'}
        },
        {
          name: 'my_bookings',
          path: 'my-bookings',
          component: MyBookings,
          meta: {title: 'View Your Bookings'}
        },
        {
          name: 'home',
          path: 'home',
          component: Dashboard,
          meta: {title: 'Welcome back'}
        },
        {
          name: 'hotel',
          path: 'hotel',
          component: HotelPage,
          meta: {title: 'Visit This Hotel'}
        }
      ]
    },
    {
      name: 'search',
      path: '/search',
      component: Search,
      children: [
        {
          name: 'normal_search',
          path: 'normal',
          component: NormalSearch,
          meta: {title: 'Search for a hotel'}
        },
        {
          name: 'advanced_search',
          component: AdvanceSearch,
          path: 'advanced',
          meta: {title: 'Advance Search'}
        }
      ]
    },
    {
      path: '*',
      component: NotFound
    }
  ]